#!/usr/bin/env bash

set -e

singularity build \
container.sif \
singularity/Singularity.$BASENAME

TAG=$(singularity inspect container.sif | grep _VERSION | awk '{print $2}')
TAG=${TAG:-$CI_COMMIT_SHORT_SHA}

singularity push \
--docker-username gitlab-ci-token \
--docker-password $CI_JOB_TOKEN \
container.sif \
oras://"$CI_REGISTRY_IMAGE"/"$BASENAME":"$TAG"