# Singularity containers

This repository contains Singularity recipes for containers of general interests to bioinformatics analysis. Containerized tools range from QC tools to trimmers, aligners and more specialized tools. Please, check the [`singularity` folder](https://git.embl.de/tabaro/singularity-containers/-/tree/master/singularity) for a complete list of available recipes.

The repository comes with a GitLab CI pipeline to automate building and pushing to a custom OCI-compliant cloud registry.

## Software dependencies

The only real requirement is Singularity/Apptainer. Please check instructions how to install it: https://apptainer.org/docs/user/main/quick_start.html

# Pull from EMBL cloud registry

All the containers should be available from the cloud registry. The repository is public, so no authentication is required to pull. Checkout the [container registry](https://git.embl.de/tabaro/singularity-containers/container_registry) for a complete list of available pre-built images. 

To pull, use the `singularity pull` command:

```bash
singularity pull alignment.sif oras://git.embl.de:4567/tabaro/singularity-containers/alignment:latest
``` 

# Build locally

If pulling is not an option, local build is automated with GNU `make`. As an alternative, one can build containers manually with the standard `singularity build` command. I will assume Singularity is already installed in the system.

## Using the provided Makefile

1. Install GNU `make` (usually done via system package manager)
2. `cd singularity`
3. `make $CONTAINER` where `CONTAINER` matches the extension of any `Singularity.*` file
4. The result will be a container with `.sif` extension in the current directory

### Example
```bash
cd singularity
make bowtie2
```

## Using standard Singularity build command

Using `singularity build` is always a viable solution:

```bash
export NAME=alignment
cd singularity
singularity build $NAME.sif Singularity.$NAME 
```

# Development

To create a new container, usually, I prefer to create a sandbox to test commands and try things out. Eventually, little by little I collect all commands needed into the final recipe. This process allows faster debug and live testing.

1. Create a sandbox from the base image, e.g. Ubuntu:
   ```bash
    singularity build --fakeroot --sandbox sandbox/ docker://ubuntu:bionic
    ```
2. Start a shell in the sandbox 
    ```bash
   singularity shell --writable sandbox/
3. Test commands and write recipe interactively
4. Name the recipe according to the conventions (`Singularity.<something>`)
5. Build a test image
   ```bash
   sudo singularity build testimage.sif Singularity.testimage
   ```
6. Update `Makefile` with new target rules