Bootstrap: docker
From: r-base:4.0.3

%labels
  AUTHOR Francesco Tabaro
  CONTACT francesco.tabaro@embl.it
  VERSION v0.2

%help
    Base: r-base 4.0.3
    Available apps: R
    Available packages: Hmisc, latticeExtra, R.utils, scales, RCurl, ggplot2, openxlsx, DESeq2, rtracklayer, topGO, ReactomePA, knitr, rmarkdown, vsn, data.table, pheatmap, apeglm, hexbin

%environment
    export LC_ALL=C
    export LC_CTYPE=C
    export LC_COLLATE=C
    export LC_TIME=C
    export LC_MESSAGES=C
    export LC_MONETARY=C
    export LC_PAPER=C
    export LC_MEASUREMENT=C

%post
    apt-get update && apt-get install -y libssl-dev libxml2-dev libcurl4-openssl-dev pandoc

    mkdir ~/.R && echo "MAKEFLAGS=-j16" > ~/.R/Makevars
    R --slave -e 'install.packages(c("hexbin", "data.table", "pheatmap", "Hmisc", "latticeExtra", "R.utils", "scales", "RCurl", "ggplot2", "BiocManager", "openxlsx", "knitr", "rmarkdown", "tidyverse"), repos="https://cloud.r-project.org/")' && \
    R --slave -e 'BiocManager::install(c("DESeq2", "rtracklayer", "topGO", "vsn", "apeglm"))' && \
    R --slave -e 'BiocManager::install("ReactomePA", method="wget")'


