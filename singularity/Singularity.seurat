Bootstrap: docker
From: rocker/tidyverse:4.1.2


%labels
  AUTHOR Francesco Tabaro
  CONTACT francesco.tabaro@embl.it
  VERSION v0.2


%help
    Base: rocker/tidyverse 4.1.2
    Available apps: R
    

%post
  apt-get update && apt-get install -y libxt-dev libigraph0-dev

  # Install a logging utility
  R --slave -e 'install.packages(c("logger", "openssl", "openxlsx"))'

  # Install Seurat
  R --slave -e 'install.packages(c("sctransform", "patchwork"))'

  # Install some extra ggplot utilities
  R --slave -e 'install.packages(c("ggrastr", "ggnewscale", "ggpubr"))'

  # Install Bioconductor packages
  R --slave -e 'install.packages("BiocManager")'
  R --slave -e 'BiocManager::install(c("glmGamPoi", "biomaRt", "limma", "MAST", "DESeq2"))'

  # Install clusterProfiler with some extras
  R --slave -e 'BiocManager::install(c("clusterProfiler", "org.Mm.eg.db", "enrichplot"))'

  # Install scMCA package
  R --slave -e 'install.packages(c("pheatmap", "shinythemes"))'
  R --slave -e 'devtools::install_github("ggjlab/scMCA", ref = "0ac6b5233547543692d61164e313e4d42514ff20")'

  # Install Seurat v4.1.0 - 21/03/2022 it is still in dev
  R --slave -e 'devtools::install_github("satijalab/seurat", ref = "v4.1.0")'

  # Install scrnasequtils 
  R --slave -e 'devtools::install_git("https://git.embl.de/tabaro/scrna-seq-utils.git", ref = "52773e420090e23691db30e063e638b619de1a33")'


%environment
    export LC_ALL=en_US-UTF8
    export LC_CTYPE=en_US-UTF8
    export LC_COLLATE=en_US-UTF8
    export LC_TIME=en_US-UTF8
    export LC_MESSAGES=en_US-UTF8
    export LC_MONETARY=en_US-UTF8
    export LC_PAPER=en_US-UTF8
    export LC_MEASUREMENT=en_US-UTF8
