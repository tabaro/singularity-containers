Bootstrap: docker
From: ubuntu:bionic

%labels
  AUTHOR Francesco Tabaro
  CONTACT francesco.tabaro@embl.it
  VERSION v0.2

%help
    Base: Ubuntu:bionic
    Available apps: Samtools, gtftogenepred (conda), genepredtobed (conda)

%post
  apt-get update && apt-get install -y curl libssl-dev

  ln -s /usr/lib/x86_64-linux-gnu/libssl.so.1.1 /usr/lib/x86_64-linux-gnu/libssl.so.1.0.0
  ln -s /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1 /usr/lib/x86_64-linux-gnu/libcrypto.so.1.0.0

  # Install Miniconda
  curl -L -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
    bash Miniconda3-latest-Linux-x86_64.sh -b -p /conda && \
    rm -f Miniconda3-latest-Linux-x86_64.sh
    export PATH="/conda/bin:${PATH}"

  conda install -y -c bioconda samtools
  conda install -y -c bioconda ucsc-gtftogenepred
  conda install -y -c bioconda ucsc-genepredtobed

%environment
  PATH="/conda/bin:${PATH}"